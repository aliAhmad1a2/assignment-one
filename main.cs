﻿using System;
using System.Drawing;
using System.Drawing.Imaging;


namespace AssignementOne
{
    class main
    {
        public static int StaticValueThreshold = 128;
        public static int mean = 0;

        static void Main(string[] args)
        {

            try
           {
               Bitmap bmp = new Bitmap("../../../Imagess/imageGray.jpg");
               
               if (Threshold.checkGrayScaleOrNot(bmp))
               {
                  mean  = Threshold.calculationMean(bmp);
                  
                  try
                  {
                      Console.WriteLine("please if need static threshold wirte s if mean Thresold wirte m");
                      string inputUser = Console.ReadLine();
                      switch (inputUser)
                      {
                          case "m":
                              Threshold.ThresholdMeanOrStatic(bmp,mean);
                              break;
                          case "s" :
                              Threshold.ThresholdMeanOrStatic(bmp,StaticValueThreshold);
                              break;
                      }

                  }
                  
                  catch (ArgumentException e )
                  {
                      Console.WriteLine(e);
                      throw;
                  }

               }
               else
               {
                   Console.WriteLine("Image is not GrayScale");
               }

           }
           catch (FileNotFoundException )
           {
               Console.WriteLine("can't read Image");
           }

        }
        
    }

}


               