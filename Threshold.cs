using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Channels;

namespace AssignementOne
{
    public class Threshold
    {

        private static int mean;

        public static bool checkGrayScaleOrNot(Bitmap bmp)
        {
            if (bmp.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                return true;
            }

            return false;
        }

        public static int calculationMean(Bitmap bmp)
        {
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    mean += bmp.GetPixel(x, y).R;
                }
            }

            mean = mean / (bmp.Width * bmp.Height);
            return mean;
        }




        public static void ThresholdMeanOrStatic(Bitmap grayScaleImage, int threshold)
        {
            Bitmap binaryImage = new Bitmap(grayScaleImage.Width, grayScaleImage.Height, PixelFormat.Format1bppIndexed);
            BitmapData grayScaleData = grayScaleImage.LockBits(
                new Rectangle(0, 0, grayScaleImage.Width, grayScaleImage.Height), ImageLockMode.ReadOnly,
                PixelFormat.Format8bppIndexed);

            BitmapData binaryData = binaryImage.LockBits(new Rectangle(0, 0, binaryImage.Width, binaryImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);

            byte thresholdValue = (byte)threshold;

            unsafe
            {
                int Height = grayScaleImage.Height;
                int Width = grayScaleImage.Width;
                for (int y = 0; y < Height; y++)
                {
                    byte * grayScaleRow = (byte *)grayScaleData.Scan0 + (y * grayScaleData.Stride);
                    byte * binaryRow = (byte *)binaryData.Scan0 + (y * binaryData.Stride);

                    for (int x = 0; x < Width; x++)
                    {

                        int binaryIndex = x / 8;
                        int binaryBit = 7 - (x % 8);

                        if (grayScaleRow[x] > thresholdValue)
                        {
                            binaryRow[binaryIndex] |= (byte)(1 << binaryBit);
                        }
                        else
                        {
                            binaryRow[binaryIndex] &= (byte)~(1 << binaryBit);
                        }
                    }
                }
            }

            grayScaleImage.UnlockBits(grayScaleData);
            binaryImage.UnlockBits(binaryData);

            binaryImage.Save("../../../Images/newImage.bmp");

        }
    }
}